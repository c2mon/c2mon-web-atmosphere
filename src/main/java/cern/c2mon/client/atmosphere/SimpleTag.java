package cern.c2mon.client.atmosphere;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import cern.c2mon.client.common.tag.Tag;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
//value={"aliveTag", "aliveTagFlag", "controlTag", "controlTagFlag", "updateTagLock"})
public interface SimpleTag extends Tag {

}
