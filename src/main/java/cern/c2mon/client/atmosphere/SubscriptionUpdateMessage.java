package cern.c2mon.client.atmosphere;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SubscriptionUpdateMessage {

	List<SubscriptionUpdateTagIdentifier> add = new ArrayList<>();
	List<SubscriptionUpdateTagIdentifier> remove = new ArrayList<>();
	List<SubscriptionUpdateTagIdentifier> query = new ArrayList<>();
    List<SimpleTag> queryResult = new ArrayList<>();
    
	
}
