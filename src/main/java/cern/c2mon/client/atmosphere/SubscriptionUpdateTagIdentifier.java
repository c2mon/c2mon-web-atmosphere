package cern.c2mon.client.atmosphere;

import java.util.HashMap;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SubscriptionUpdateTagIdentifier {
   Long id;
   String name;
   String metakey;
   String metavalue;
}
