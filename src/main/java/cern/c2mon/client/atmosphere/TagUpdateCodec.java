package cern.c2mon.client.atmosphere;

import java.io.IOException;

import org.atmosphere.config.managed.Decoder;
import org.atmosphere.config.managed.Encoder;

import com.fasterxml.jackson.databind.ObjectMapper;

import cern.c2mon.client.common.tag.Tag;
import cern.c2mon.shared.client.tag.TagUpdate;

public class TagUpdateCodec implements
		Encoder<Tag, String>, Decoder<String, Tag> {

	private static final ObjectMapper mapper = new ObjectMapper();

	@Override
	public String encode(Tag m) {
		try {
			return mapper.writeValueAsString(new SimpleTagImpl(m));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public Tag decode(String m) {
		try {
			return mapper.readValue(m, Tag.class);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
