package cern.c2mon.client.atmosphere;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import org.atmosphere.config.managed.Decoder;
import org.atmosphere.config.managed.Encoder;
import org.atmosphere.config.service.DeliverTo;
import org.atmosphere.config.service.Disconnect;
import org.atmosphere.config.service.Get;
import org.atmosphere.config.service.ManagedService;
import org.atmosphere.config.service.Message;
import org.atmosphere.config.service.Ready;
import org.atmosphere.config.service.Resume;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.atmosphere.cpr.Broadcaster;
import org.atmosphere.cpr.BroadcasterFactory;
import org.atmosphere.cpr.BroadcasterListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;

import cern.c2mon.client.common.listener.TagListener;
import cern.c2mon.client.common.tag.Tag;
import cern.c2mon.client.core.service.TagService;

/**
 * This Atmosphere service accepts subscription requests via GET, POST or
 * WebSocket messages. It connects C2MON tags to Atmosphere broadcasters to
 * allow the forwarding of data.
 * 
 * @author bcopy
 */
@ManagedService(path = "/broadcast/{view: [a-zA-Z][a-zA-Z_0-9]*}")
public class C2MonBroadcastService extends BroadcasterListenerAdapter {
    private final Logger logger = LoggerFactory.getLogger(C2MonBroadcastService.class);

    private static TagUpdateCodec tagUpdatesCodec = new TagUpdateCodec();

    @Autowired
    TagService c2monTagService;

    private AtomicBoolean initialized = new AtomicBoolean();

    private BroadcasterFactory broadcasterFactory;
    
    private Broadcaster broadcaster;

    private final ObjectMapper mapper = new ObjectMapper();

    public C2MonBroadcastService() {
    }

    @Ready
    public void onReady(final AtmosphereResource resource, Broadcaster bcaster) {
        if (this.logger.isInfoEnabled()) {
            this.logger.info("Connected {}", resource.uuid());
        }
        obtainBroadcasterFactory(resource);
        
    }

    private void obtainBroadcasterFactory(final AtmosphereResource resource) {
        if (!initialized.getAndSet(true)) {
            broadcasterFactory = resource.getAtmosphereConfig().getBroadcasterFactory();
            broadcasterFactory.addBroadcasterListener(this);
            broadcaster = resource.getBroadcaster();
        }
    }

    @Disconnect
    public void onDisconnect(AtmosphereResourceEvent event) {
        if (logger.isInfoEnabled()) {
            this.logger.info("Client {} disconnected [{}]", event.getResource().uuid(),
                    (event.isCancelled() ? "cancelled" : "closed"));
        }
    }

    @Resume
    public void onTimeout(AtmosphereResourceEvent e) {
        if (e.isResumedOnTimeout()) {
            try {
                e.getResource().close();
            } catch (IOException e1) {
                this.logger.warn("Exception while closing resource : ", e1);
            }
        }
    }

    @Get
    public void onGetSubscribe(AtmosphereResource resource) {
        obtainBroadcasterFactory(resource);
        String[] subscriptionsIdToAdd = resource.getRequest().getParameterValues("id");
        List<SubscriptionUpdateTagIdentifier> tagIdentifiersById = new ArrayList<SubscriptionUpdateTagIdentifier>();
        if (subscriptionsIdToAdd.length > 0) {
            tagIdentifiersById = Arrays.asList(subscriptionsIdToAdd).stream()
                    .map(id -> SubscriptionUpdateTagIdentifier.builder().id(Long.valueOf(id)).build())
                    .collect(Collectors.toList());

            String[] subscriptionsNamesToAdd = resource.getRequest().getParameterValues("name");

            List<SubscriptionUpdateTagIdentifier> tagIdentifiersByName = Arrays.asList(subscriptionsNamesToAdd).stream()
                    .map(name -> SubscriptionUpdateTagIdentifier.builder().name(name).build())
                    .collect(Collectors.toList());

            tagIdentifiersById.addAll(tagIdentifiersByName);
        }
        resource.suspend();

        if (!tagIdentifiersById.isEmpty()) {
            subscribeTagsByTagIdentifier(resource, tagIdentifiersById);
        }
    }

    public Set<SimpleTag> queryTagInformation(AtmosphereResource resource,
            List<SubscriptionUpdateTagIdentifier> tagIdentifiersList) {
        Set<Long> tagsIdsToQuery = convertTagIdentifiersToTagIDs(
                tagIdentifiersList.stream().filter(ident -> ident != null && ident.id != null)
                        .map(SubscriptionUpdateTagIdentifier::getId).collect(Collectors.toSet())
                ,tagIdentifiersList.stream().filter(ident -> ident != null && ident.name != null)
                        .map(SubscriptionUpdateTagIdentifier::getName).collect(Collectors.toSet())
                ,tagIdentifiersList.stream().filter(ident -> ident != null && ident.metakey != null && ident.metavalue != null)
                        .map(t -> { return new AbstractMap.SimpleEntry<String,String>(t.getMetakey(), t.getMetavalue()); }).collect(Collectors.toList()));
        Set<SimpleTag> queryResult = new HashSet<>();
        try {

            for (Tag tagToAdd : c2monTagService.get(tagsIdsToQuery)) {
                queryResult.add(new SimpleTagImpl(tagToAdd));
            }
        } catch (Exception e) {
            this.logger.warn("Failed tag query by ID for resource {} : {}", resource, e);
        }
        return queryResult;
    }

    public Collection<SubscriptionUpdateTagIdentifier> subscribeTagsByTagIdentifier(AtmosphereResource resource,
            List<SubscriptionUpdateTagIdentifier> tagIdentifiersList) {
        Set<Long> tagsIdsToSubscribe = convertTagIdentifiersToTagIDs(
                tagIdentifiersList.stream().filter(ident -> ident != null && ident.id != null)
                        .map(SubscriptionUpdateTagIdentifier::getId).collect(Collectors.toSet()),
                tagIdentifiersList.stream().filter(ident -> ident != null && ident.name != null)
                        .map(SubscriptionUpdateTagIdentifier::getName).collect(Collectors.toSet())
                ,null);

        Set<Long> validTagIds = new HashSet<>();
        try {
//            for (Long tagToAdd : tagsIdsToSubscribe) {
//                if (this.logger.isDebugEnabled()) {
//                    this.logger.debug("Associating res {} with tag id {} ", resource.uuid(), tagToAdd);
//                }
//                broadcasterFactory.lookup(tagToAdd, true).addAtmosphereResource(resource);
//                validTagIds.add(tagToAdd);
//            }
            broadcaster.addAtmosphereResource(resource);
            if (!tagsIdsToSubscribe.isEmpty()) {
                c2monTagService.subscribe(tagsIdsToSubscribe, broadcastingTagListener);
            } else {
                if (this.logger.isDebugEnabled()) {
                    this.logger.debug("Subscription request with an empty list !");
                }
            }
        } catch (Exception e) {
            this.logger.warn("Failed tag subscription by ID : ", e);
        }

        return validTagIds.stream().map(tagId -> SubscriptionUpdateTagIdentifier.builder().id(tagId).build())
                .collect(Collectors.toSet());
    }

    public Set<Long> unsubscribeTagsByTagIdentifier(AtmosphereResource resource,
            List<SubscriptionUpdateTagIdentifier> tagIdentifiersList) {
        Set<Long> tagsIdsToUnsubscribe = convertTagIdentifiersToTagIDs(
                tagIdentifiersList.stream().filter(ident -> ident != null && ident.id != null)
                        .map(SubscriptionUpdateTagIdentifier::getId).collect(Collectors.toSet())
                ,tagIdentifiersList.stream().filter(ident -> ident != null && ident.name != null)
                        .map(SubscriptionUpdateTagIdentifier::getName).collect(Collectors.toSet())
                ,null);

        Set<Long> validTagIds = new HashSet<>();
        try {
            for (Long tagID : tagsIdsToUnsubscribe) {
                if (this.logger.isDebugEnabled()) {
                    this.logger.debug("Associating res {} with tag id {} ", resource.uuid(), tagID);
                }
//                Broadcaster bcaster = broadcasterFactory.lookup(tagID, false);
//                if (bcaster != null) {
//                    bcaster.removeAtmosphereResource(resource);
//                }
                validTagIds.add(tagID);
            }
            c2monTagService.unsubscribe(tagsIdsToUnsubscribe, broadcastingTagListener);
        } catch (Exception e) {
            this.logger.warn("Failed tag unsubscription by ID : ", e);
        }

        return validTagIds;
    }

    private final Set<Long> convertTagIdentifiersToTagIDs(final Set<Long> tagIdsList
             , final Set<String> tagNamesList
             , final List<Map.Entry<String,String>> metadataPairs) {
        Set<Long> result = new HashSet<>();

        if ((tagIdsList != null) && !tagIdsList.isEmpty()) {
            if (logger.isDebugEnabled()) {
                this.logger.debug("Looking up tags IDs {}",
                        tagIdsList.stream().map(Object::toString).collect(Collectors.joining(",")));
            }
            result.addAll(tagIdsList);
        }

        {
            if ((tagNamesList != null) && !tagNamesList.isEmpty() && logger.isDebugEnabled()) {
                this.logger.debug("Looking up tags Names {}", tagNamesList.stream().collect(Collectors.joining(",")));
            }
            // Convert all tag names to corresponding tag IDs
            result.addAll(convertTagNameToIds(tagNamesList));
        }
        
        if(metadataPairs!=null)
        {
            for(Map.Entry<String,String> pair : metadataPairs) {
                if(pair != null && pair.getKey() != null) {
                    try {
                      result.addAll(convertMetadataToTagIds(pair.getKey(), pair.getValue()));
                    }catch(Exception e) {
                        this.logger.error("Invalid metadata query {}, exception : {}", pair.toString(), e );
                    }
                }
            }
        }
        
        return result;
    }

    private final Collection<Long> convertTagNameToIds(final Set<String> tagNamesList) {
        Collection<Tag> tags = c2monTagService.findByName(tagNamesList);
        return tags.parallelStream().map(tag -> tag.getId()).collect(Collectors.toList());
    }
    
    private final Collection<Long> convertMetadataToTagIds(final String key, final String value) {
        Collection<Tag> tags = c2monTagService.findByMetadata(key, value);
        return tags.parallelStream().map(tag -> tag.getId()).collect(Collectors.toList());
    }


    @Message(encoders = SubscriptionUpdateMessageEncDec.class, decoders = SubscriptionUpdateMessageEncDec.class)
    @DeliverTo(DeliverTo.DELIVER_TO.RESOURCE)
    public SubscriptionUpdateMessage onMessage(AtmosphereResource resource, SubscriptionUpdateMessage message) {
        resource.suspend();
        SubscriptionUpdateMessage reply = new SubscriptionUpdateMessage();
        if (!message.query.isEmpty()) {
            reply.queryResult
                    .addAll(queryTagInformation(resource, message.query).stream().collect(Collectors.toList()));
        } else if (!message.add.isEmpty()) {
            subscribeTagsByTagIdentifier(resource, message.add);
        }
        else if (!message.remove.isEmpty()) {
            unsubscribeTagsByTagIdentifier(resource, message.add);
        }

        return reply;
    }

    @Message(encoders = TagUpdateListCodec.class, decoders = TagUpdateListCodec.class)
    public Collection<Tag> onMessage(AtmosphereResource resource, Collection<Tag> tagUpdate) {
        return tagUpdate;
    }

    public final class SubscriptionUpdateMessageEncDec
            implements Encoder<SubscriptionUpdateMessage, String>, Decoder<String, SubscriptionUpdateMessage> {

        @Override
        public String encode(SubscriptionUpdateMessage m) {
            try {
                return mapper.writeValueAsString(m);
            } catch (IOException ex) {
                logger.warn("Unable to encode message : ", ex);
                throw new IllegalStateException(ex);
            }
        }

        @Override
        public SubscriptionUpdateMessage decode(String s) {
            try {
                return mapper.readValue(s, SubscriptionUpdateMessage.class);
            } catch (IOException ex) {
                logger.warn("Unable to decode message : ", ex);
                throw new IllegalStateException(ex);
            }
        }

    }

    /*
     * // @Post // public void subscribeByURI(AtmosphereResource resource) { //
     * obtainBroadcasterFactory(resource); // String[] subscriptionsToAdd =
     * resource.getRequest().getParameterValues("uri"); //
     * addSubscriptionURIsToResource(resource, subscriptionsToAdd); // // }
     * 
     * // private void addSubscriptionURIsToResource(AtmosphereResource
     * resource, String[] subscriptionsToAdd) { // Set<String> uris =
     * Arrays.stream(subscriptionsToAdd).collect(Collectors.toSet()); // // for
     * (String uri : uris) { // try { // Tag tag =
     * c2monDynConfigService.getTagForURI(new URI(uri)); //
     * c2monTagService.subscribe(tag.getId(), broadcastingTagListener); // //
     * m_broadcasterFactory.lookup(tag.getId(),
     * true).addAtmosphereResource(resource.suspend()); // } catch
     * (URISyntaxException ex) { //
     * this.logger.error("Could not subscribe to {} : syntax exception",
     * uri.toString(), ex); // } // } // // c2monTagService.refresh(); // }
     * 
     */

    TagListener broadcastingTagListener = new TagListener() {
        @Override
        public void onUpdate(Tag tagUpdate) {
            onInitialUpdate(Arrays.asList(tagUpdate));
        }

        @Override
        public void onInitialUpdate(Collection<Tag> initialValues) {
//            initialValues.parallelStream().forEach(tag -> {
//                Broadcaster b = broadcasterFactory.lookup(tag.getId(), false);
//                if (b != null) {
//                    b.broadcast(tag);
//                }
//            });
            broadcaster.broadcast(initialValues);
        }
    };

//    /**
//     * BroadcasterListener implementation that cleans up any tag subscriptions
//     * when the broadcaster is removed.
//     * 
//     * @param broadcaster
//     *            The broadcaster about to be destroyed.
//     */
//    @Override
//    public void onPreDestroy(Broadcaster b) {
//        try {
//            c2monTagService.unsubscribe(Long.valueOf(b.getID()), broadcastingTagListener);
//        } catch (Exception e) {
//            this.logger.warn("Could not unsubscribe from tag {} upon broadcaster destroy, cause : {} ", b.getID(), e);
//        }
//    }

}
