package cern.c2mon.client.atmosphere;

import java.io.IOException;
import java.util.List;

import org.atmosphere.config.managed.Decoder;
import org.atmosphere.config.managed.Encoder;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import cern.c2mon.client.common.tag.Tag;

public class TagUpdateListCodec implements
		Encoder<List<Tag>, String>, Decoder<String, List<Tag>> {

	private static final ObjectMapper mapper = new ObjectMapper();

	@Override
	public String encode(List<Tag> list) {
		try {
			return mapper.writeValueAsString(list);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public List<Tag> decode(String m) {
		try {
			return mapper.readValue(m, new TypeReference<List<Tag>>() {});
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
