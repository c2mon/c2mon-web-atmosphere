package cern.c2mon.client.atmosphere;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cern.c2mon.client.common.tag.Tag;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Delegate;

@Data
@Builder
@NoArgsConstructor
public class SimpleTagImpl implements SimpleTag, Serializable {
	/**
	 * Generated serialization UID
	 */
	private static final long serialVersionUID = -476267784368913029L;

	@JsonIgnore
	@Delegate(types=SimpleTag.class)
	Tag tag;
	
	public SimpleTagImpl(Tag tag){
		this.tag = tag;
	}
	

}
