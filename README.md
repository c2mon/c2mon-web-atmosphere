# C2MON Web Atmosphere Connector

This Spring Boot application acts as a bridge between a C2MON server cluster and Web clients, by allowing dynamic tag subscription and forwarding of data.
